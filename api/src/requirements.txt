aio-pika==9.1.3
fastapi==0.98.0
loguru==0.7.0
uvicorn==0.22.0