import json

from fastapi import FastAPI
from loguru import logger

from rmq.publisher import publish
from settings import TASKS_EXCHANGE_NAME

app = FastAPI()


def call_remotely(func):
    async def wrapper():
        logger.info("Start calling remote function: '%s'" % func.__name__)
        data = json.dumps({"name": func.__name__})
        await publish(data, to=TASKS_EXCHANGE_NAME)
    return wrapper


@call_remotely
async def ping():
    logger.info(f"Start parameters validation (func=ping)")
    # Some validation here