from loguru import logger

from app import app


@app.on_event("startup")
async def startup():
    logger.info(f"Started labyrinth app.")



