import asyncio

from app import ping

loop = asyncio.get_event_loop()
loop.run_until_complete(ping())