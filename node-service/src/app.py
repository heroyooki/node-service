from fastapi import FastAPI

from loguru import logger


app = FastAPI()


async def ping():
    logger.info("pong")


async def process_task(data):
    try:
        await eval(data["name"])()
    except NameError:
        logger.error(f"Received invalid procedure name (data={data})")