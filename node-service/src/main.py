import asyncio

from loguru import logger

from rmq.consumer import start_consume
from settings import TASKS_EXCHANGE_NAME
from app import app, process_task

background_tasks = set()


@app.on_event("startup")
async def startup():
    logger.info(f"Started node service.")

    task = asyncio.create_task(
        start_consume(
            TASKS_EXCHANGE_NAME,
            on_message=process_task)
    )
    background_tasks.add(task)